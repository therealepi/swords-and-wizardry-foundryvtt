# Swords and Wizardry FoundryVTT

An implementation of the Swords & Wizardry game system for [Foundry Virtual Tabletop](http://foundryvtt.com).

The software component of this system is distributed under the GNUv3 license while the game content is distributed under the Open Gaming License v1.0a.

“Swords & Wizardry, S&W, and Mythmere Games are trademarks of Matthew J. Finch,” and I am not affiliated with Matthew J. Finch, Mythmere Games™, or Frog God Games